import React, { useState } from "react";
import propTypes from "prop-types";

export default function TodoForm({ onSubmit, title, edit, cancel }) {
  const [input, setInput] = useState(edit ? edit.value : "");

  const handleChange = (e) => {
    setInput(e.target.value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    onSubmit({
      id: Math.floor(Math.random() * 10000),
      text: input,
    });

    setInput("");
  };
  return (
    <>
      <form className="todo-form" onSubmit={handleSubmit}>
        {edit ? (
          <>
            <input
              type="text"
              placeholder={title ?? "Please change placeholder"}
              value={input}
              name="text"
              className="todo-input edit"
              onChange={handleChange}
            />
            <button className="todo-button edit">Edit</button>
            <button
              type="button"
              className="todo-button cancel"
              onClick={cancel}
            >
              Cancel
            </button>
          </>
        ) : (
          <>
            <input
              type="text"
              placeholder={title ?? "Please change placeholder"}
              value={input}
              name="text"
              className="todo-input"
              onChange={handleChange}
            />
            <button className="todo-button">Add</button>
          </>
        )}
      </form>
    </>
  );
}

TodoForm.propTypes = {
  title: propTypes.string,
};
