import React from 'react'
const btn = {
  padding: '15px 30px'
}

export default function Counter(props) {
  const {title, action} = props
  return (
    <button style={btn} onClick={action}>{title}</button>
  )
}
